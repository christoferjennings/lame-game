#ifndef GAMEENGINE_H
#define GAMEENGINE_H

#include <SDL.h>

// GameEngine supports the game-loop lifecycle.
class GameEngine {
public:
    GameEngine();
    ~GameEngine();

    // Initializes the game and screen.
    void Init(const char* title, int xpos, int ypos, int width, int height, bool fullscreen);

    // Handle global events.
    void HandleEvents();

    // Update game state.
    void Update();

    // Render things to the screen.
    void Render();

    // Clean up before ending the game.
    void Clean();

    // Indicates if the game is currently running.
    bool Running();

private:
    bool isRunning;
    SDL_Window *window;
    SDL_Renderer *renderer;
};

#endif //GAMEENGINE_H
