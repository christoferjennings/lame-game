#include <SDL_keyboard.h>
#include <iostream>
#include <SDL_image.h>
#include "Monster.h"

using std::cout;
using std::endl;

Monster::Monster(SDL_Renderer *renderer, int x, int y, double scale)
: Character(renderer, x, y, scale) {
    //cout << "Monster ctor" << endl;

    SDL_Surface *surface = IMG_Load("assets/eye.png");
    texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);
}

Monster::~Monster() {
    //cout << "Monster DESTRUCTOR" << endl;
}

void Monster::Update() {
    //cout << "Monster::Update" << endl;
    static int step = 3;
    const Uint8* keyboardState = SDL_GetKeyboardState(nullptr );

    // Move
    if ( keyboardState[SDL_SCANCODE_UP]) {
        ypos = ypos - step;
    }
    if (keyboardState[SDL_SCANCODE_DOWN]) {
        ypos = ypos + step;
    }
    if (keyboardState[SDL_SCANCODE_LEFT]) {
        xpos = xpos - step;
    }
    if (keyboardState[SDL_SCANCODE_RIGHT]) {
        xpos = xpos + step;
    }

    // Scale
    if (keyboardState[SDL_SCANCODE_MINUS]) {
        scale = scale * 0.97;
    }
    if (keyboardState[SDL_SCANCODE_EQUALS]) {
        scale = scale * 1.03;
    }

    Character::Update();
}
