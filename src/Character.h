#ifndef CHARACTER_H
#define CHARACTER_H

#include <SDL_render.h>
#include "GameObject.h"

// Character is a type of GameObject that moves around.
// Currently this game just has two Character types: Player and Monster.
// These share common approaches to movement, scaling, and rendering which is defined in Character.
class Character : public GameObject {
public:
    Character(SDL_Renderer *renderer, int x, int y, double scale);
    virtual ~Character();

    // Update the state of this game object.
    virtual void Update();

    // Render this game object to reflect the current state. (Called after Update.)
    virtual void Render();

protected:
    int xpos;
    int ypos;
    double scale;
    SDL_Texture *texture;
    SDL_Rect srcRect;
    SDL_Rect destRect;
};

#endif //CHARACTER_H
