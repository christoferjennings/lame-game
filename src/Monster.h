#ifndef MONSTER_H
#define MONSTER_H

#include "Character.h"

// Monster renders as an eye.
// Use the arrow keys to move Monster. Use the - and = keys to make the monster change size.
// All Monsters do the same thing in this version. They all move together. ... Yup. It's a lame game!
class Monster final : public Character {
public:
    // Note that Monster a scale in the constructor but not Player.
    // This is just so I could make Monsters different sizes.
    Monster(SDL_Renderer *renderer, int x, int y, double scale);
    ~Monster();

    void Update() override;

    // Note that Monster doesn't need Render because it's in Character.
};

#endif //MONSTER_H
