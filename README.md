# Lame Game

This is the capstone project for the Udacity C++ nanodgree program.

There is no point to this game except to be lame while also passing the course. 

The game is built on [SDL2](https://www.libsdl.org/).

## Setup

Install CMake and SDL libraries.

On macOS using brew...
~~~bash
brew install git
brew install git-lfs
brew install cmake
brew install sdl2
brew install sdl2_image
brew install sdl2_ttf
~~~

On Ubuntu using apt (my VM crashed but I'm pretty sure this is complete)...
~~~bash
sudo apt install git
sudo apt install git-lfs
sudo apt install cmake
sudo apt install libsdl2-dev
sudo apt install libsdl2-image-2.0-0
sudo apt install libsql2-image-dev
sudo apt install libsdl2-ttf-2.0-0 
sudo apt install libsdl2-ttf-dev
~~~

## CLone this Repository

This project uses git LFS so...

~~~bash
git clone https://gitlab.com/christoferjennings/lame-game.git
cd lame-game
git lfs fetch
~~~

## To Build and Run

After you clone this project, you need to

In the project's root directory...
~~~bash
mkdir build
cd build
cmake -H.. -B.
make
./lame-game
~~~

## Game Description

The game is lame. so it's called lame-game. 

![screenshot](screenshot.png)

The score of the game is simply how long you leave it running. Nothing in the game can cause you to lose.
The point wasn't to make a good game but just to demonstrate C++ skills. The score is just a sum of frames 
run in the game. The longer the game runs the higher the score.                                                                         The point wasn't to make a good game. The score demonstrates reading and writing to a file.

The "Player" in the game is a coffee cup. You can move it around with the `W`, `A`, `S`, and `D` keys. That's all it can do.

The "Monsters" in the game are eyes. One is larger than the other just to demonstrate having different
constructor signatures. You can move the monsters around with the arrow keys. You can make the monsters 
grow and shrink using the `-` and `=` keys.

## Class Structure

This diagram shows the basic class structure of the game's code.

![classes](classes.png)

`main` sets up the game and runs the game loop.

`GameEngine` implements lifecycle functions and delegates to `GameObject` objects 
to render things on the screen.

`GameObject` is pure-virtual (interface in UML), defining a polymorphic base type.

`Character` defines common fields and functions for `Player` and `Monster`.

`Score` has different needs than `Player` and `Monster`, so it directly implements `GameObject`.

## Platforms

I developed this on macOS Mojave and macOS Catalina and verified it in an Ubuntu 18 VM.  
After upgrading to macOS Catalina I had to upgrade VMware to 11.5 but then had trouble getting
the score text to display in Ubuntu on the VM. 
Then my VM setup crashed so I'm trying to get tech support from VMware.

## Building with CMake

I used CMake for the build system. It integrates with CLion too, which I used as my editor.

## Capstone Project Rurik 

The instructions state...
> Ensure that the application you build will satisfy all criteria for the “README” and “Compiling and Testing” sections, and that the application will satisfy at least 5 total criteria from the rest of the rubric. Not all rubric items need to be satisfied in order to pass the project. Scope the features of your project to meet the rubric criteria you want to target.

The "**at lease 5**" are all from the OOP category.

### Compiling and Testing (All Rubric Points REQUIRED)

| Criteria | How it's met |
| :------- | :----------- |
| The submission must compile and run. | Uses CMake for a cross-platform build. See instructions above. |

### Loops, Functions, I/O

| Criteria | How it's met |
| :------- | :----------- |
| The project demonstrates an understanding of C++ functions and control structures. | `main.cpp` has the game loop. Conditionals are used in multiple places. Functions are used.  |
| The project reads data from a file and process the data, or the program writes data to a file. | `Score.cpp`'s constructor reads the `high-score` file if it's available on startup. On shutdown, the destructor compares the user's score to the high score and replaces the high score if appropriate. |
| The project accepts user input and processes the input. | Keyboard events are captured in `GameEngine::HandleEvents()`, `Player::Update()`, and `Monster::Update()`. |

### Object Oriented Programming

| Criteria | How it's met |
| :------- | :----------- |
| The project uses Object Oriented Programming techniques. | See [class diagram](#class-structure). |
| Classes use appropriate access specifiers for class members. | No private functions were necessary so they are all public. Fields are private or protected as appropriate for each case. |
| Class constructors utilize member initialization lists. | See each class constructor. |
| Classes abstract implementation details from their interfaces. | `.h` header files are separate from `.cpp` implementations. Also, `GameObject` is pure-virtual. |
| Classes encapsulate behavior. | Each class is responsible for it's own behavior, which cannot be modified by other classes. |
| Classes follow an appropriate inheritance hierarchy. | See the [class diagram](#class-structure). Specifically, the `GameObject` hierarchy. |
| Derived class functions override virtual base class functions. | See the [class diagram](#class-structure). Specifically, the `GameObject` hierarchy. |

## References and Thanks

Many thanks to all the people wo've posted tutorials online and posted things to places 
like http://stackoverflow.com

#### Tutorials

- [Lazy Foo Productions](http://lazyfoo.net/tutorials/SDL/)
- I followed this playlist a lot: 
  [How to Make a Game in C++ and SDL2 from Scratch](https://www.youtube.com/playlist?list=PLhfAbcv9cehhkG7ZQK0nfIGJC_C-wSLrx).
  I deviated from it when its approach was more than I needed.

#### Other Stuff

- macOS Mojave didn't show a window if just using a `SDL_Delay`. Had
  to use a loop. Thanks goes to
  [this](https://stackoverflow.com/a/41044089).
- I like the design used in [this tutorial](https://youtu.be/44tO977slsU)
- Free icons used thanks to https://www.iconfinder.com/iconsets/32x32-free-design-icons
- I got the `FindSDL2.cmake` and `FindSDL2_image.cmake` from  https://github.com/trenki2/SDL2Test
- I made `FindSDL2_ttf.cmake` as a copy of `FindSDL2_image.cmake` then search/replaced all "image" with "ttf".
