#ifndef PLAYER_H
#define PLAYER_H

#include "Character.h"

// Player is the coffee cup at the bottom of the screen.
// Use the WASD keys to move Player. ... That it. It's a lame game! :)
class Player final : public Character {
public:
    // Note that Player doesn't take a scale in the constructor, Monster does though.
    Player(SDL_Renderer *renderer, int x, int y);
    ~Player();

    void Update() override;

    // Note that Monster doesn't need Render because it's in Character.
};

#endif //PLAYER_H
