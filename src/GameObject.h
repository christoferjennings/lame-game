#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <SDL_render.h>

// GameObject is a pure-virtual base type.
// GameEngine polymorphically calls subtypes to update and render themselves.
class GameObject {
public:
    GameObject(SDL_Renderer *renderer);
    virtual ~GameObject();

    // Update the state of this game object.
    virtual void Update() = 0;

    // Render this game object to reflect the current state. (Called after Update.)
    virtual void Render() = 0;

protected:
    // The SDL_Renderer is used by each GameObject and so is made available here.
    SDL_Renderer *renderer;
};

#endif //GAMEOBJECT_H
