#include <SDL_keyboard.h>
#include <iostream>
#include <SDL_image.h>
#include "Player.h"

using std::cout;
using std::endl;

Player::Player(SDL_Renderer *renderer, int x, int y)
: Character(renderer, x, y, 2.0) {
    //cout << "Player ctor" << endl;

    SDL_Surface *surface = IMG_Load("assets/coffee.png");
    texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);
}

Player::~Player() {
    //cout << "Player DESTRUCTOR" << endl;
}

void Player::Update() {
    //cout << "Player::Update" << endl;
    static int step = 3;
    const Uint8* keyboardState = SDL_GetKeyboardState(nullptr );
    if ( keyboardState[SDL_SCANCODE_W]) {
        ypos = ypos - step;
    }
    if (keyboardState[SDL_SCANCODE_S]) {
        ypos = ypos + step;
    }
    if (keyboardState[SDL_SCANCODE_A]) {
        xpos = xpos - step;
    }
    if (keyboardState[SDL_SCANCODE_D]) {
        xpos = xpos + step;
    }

    Character::Update();
}
