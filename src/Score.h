#ifndef SCORE_H
#define SCORE_H
#include "GameObject.h"

// Score is not a Character, it's just a GameObject because it doesn't move or scale.
// ... Just an excuse to make class hierarchies interesting.
//
// The high score and current score are displayed.
// If the current score is higher that the high score when the game closes then it becomes the new high score.
// To reset the high score, delete the high-score file in the build directory and run the game again.
class Score final : public GameObject {
public:
    Score(SDL_Renderer *renderer);
    ~Score();

    void Update() override;
    void Render() override;

private:
    const SDL_Color COLOR = {0,0,0};

    SDL_Rect destRect;

    u_int score;
    u_int highScore;

    TTF_Font *font;
    SDL_Texture *texture;
};

#endif //SCORE_H
