//Using SDL and standard IO
#include <SDL.h>
#include "GameEngine.h"
#include <iostream>

using std::cout;
using std::endl;

//Screen dimension constants
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

// Game speed constants
const int FRAMES_PER_SECOND = 60;
const int FRAME_DELAY = 1000 / FRAMES_PER_SECOND;

GameEngine *game = nullptr;

int main( int argc, char* args[] ) {
    game = new GameEngine();

    game->Init("My Lame Game",
               SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
               SCREEN_WIDTH, SCREEN_HEIGHT, false);

    Uint32 frameStart;
    u_int frameTime;

    // This is the game loop. It's basically a copy of the Lazy Foo tutorial.
    while (game->Running()) {
        frameStart = SDL_GetTicks();

        game->HandleEvents();
        game->Update();
        game->Render();

        // Frame delay to keep game speed the same across machines.
        frameTime = SDL_GetTicks() - frameStart;
        if (FRAME_DELAY > frameTime) {
            SDL_Delay(FRAME_DELAY - frameTime);
        }
    }

    game->Clean();

    return 0;
}
