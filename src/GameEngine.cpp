#include <iostream>
#include <istream>
#include <fstream>
#include <vector>
#include <SDL_ttf.h>
#include "GameEngine.h"
#include "Player.h"
#include "Monster.h"
#include "Score.h"

using std::cout;
using std::endl;

std::vector<GameObject*> objs;

GameEngine::GameEngine() {
}

GameEngine::~GameEngine() {
}

void GameEngine::Init(const char *title, int xpos, int ypos, int width, int height, bool fullscreen) {
    int flags = fullscreen ? SDL_WINDOW_FULLSCREEN : 0;

    isRunning = false;
    if (SDL_Init(SDL_INIT_EVERYTHING) == 0) {
        cout << "Subsystems initialized." << endl;

        window = SDL_CreateWindow(title, xpos, ypos, width, height, flags);
        if (window) cout << "Window created." << endl;

        renderer = SDL_CreateRenderer(window, -1, 0);
        if (renderer) {
            SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
            cout << "Renderer created." << endl;
        }

        isRunning = true;
    }

    objs.push_back(new Player(renderer, (width * 0.5 - 32), (height - 80)));

    objs.push_back(new Monster(renderer, (width * 0.5 - 32), 80, 2.0));
    objs.push_back(new Monster(renderer, (width * 0.5 + 64), 80, 2.5));

    objs.push_back(new Score(renderer));
}

void GameEngine::HandleEvents() {
    SDL_Event event;
    SDL_PollEvent(&event);
    switch (event.type) {
        case SDL_QUIT:
            isRunning = false;
            break;
        default:
            break;
    }
}

void GameEngine::Update() {
    //cout << "GameEngine::Update --------" << endl;
    for(GameObject *o : objs) {
        o->Update();
    }
}

void GameEngine::Render() {
    SDL_RenderClear(renderer);
    for(GameObject *o : objs) {
        o->Render();
    }
    SDL_RenderPresent(renderer);
}

void GameEngine::Clean() {
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);
    SDL_Quit();
    TTF_Quit();
    for(GameObject *o : objs) {
        delete o;
    }
    objs.clear();
    cout << "Game cleaned." << endl;
}

bool GameEngine::Running() {
    return isRunning;
}
