#include <SDL_render.h>
#include "GameObject.h"
#include <iostream>

using std::cout;
using std::endl;

GameObject::GameObject(SDL_Renderer *renderer)
: renderer(renderer) {
    //cout << "GameObject ctor" << endl;
}

GameObject::~GameObject() {
    //cout << "GameObject DESTRUCTOR" << endl;
}
