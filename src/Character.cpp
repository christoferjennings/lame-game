#include <SDL_render.h>
#include "Character.h"
#include <iostream>

using std::cout;
using std::endl;

Character::Character(SDL_Renderer *renderer, int x, int y, double scale)
: GameObject(renderer), xpos(x), ypos(y), scale(scale) {
    //cout << "Character ctor" << endl;

    srcRect.x = 0;
    srcRect.y = 0;
    srcRect.w = 32;
    srcRect.h = 32;

//    destRect.x = xpos;
//    destRect.y = ypos;
//    destRect.w = srcRect.w * scale;
//    destRect.h = srcRect.h * scale;

    texture = nullptr;
}

Character::~Character() {
    //cout << "Character DESTRUCTOR" << endl;
}

void Character::Update() {
    //cout << "Character::Update" << endl;

    destRect.x = xpos;
    destRect.y = ypos;
    destRect.w = srcRect.w * scale;
    destRect.h = srcRect.h * scale;
}

void Character::Render() {
    //cout << "Character::Render" << endl;
    SDL_RenderCopy(renderer, texture, &srcRect, &destRect);
}
