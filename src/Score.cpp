#include <SDL_keyboard.h>
#include <iostream>
#include <fstream>
#include <SDL_ttf.h>
#include "Score.h"

using std::cout;
using std::endl;

static int SCORE_RECT_WIDTH = 100;

Score::Score(SDL_Renderer *renderer)
: GameObject(renderer) {
    //cout << "Score ctor" << endl;

    destRect.x = 5;
    destRect.y = 5;

    // Score always starts at zero.
    score = 0;

    // Load the previous high score from file.
    std::ifstream scoreFile("high-score");
    if (scoreFile) {
        scoreFile >> highScore;
    } else {
        highScore = 0;
    }

    // Initialize SDL_ttf
    TTF_Init();

    // Load font.
//    font = TTF_OpenFont("assets/lazy.ttf", 20);
//    font = TTF_OpenFont("assets/Garuda.ttf", 20);
    font = TTF_OpenFont("assets/Verdana.ttf", 20);
    if (!font) {
        cout << "Score failed to open font: " << TTF_GetError << endl;
    }
}

Score::~Score() {
    //cout << "Score DESTRUCTOR. Score = " << score << endl;

    if (score > highScore) {
        // Save new high score to file.
        std::ofstream scoreFile("high-score");
        scoreFile << score;
    }
}

void Score::Update() {
    //cout << "Score::Update" << endl;

    // Increase the score. (Yes. This is a lame scoring mechanism. It's a lame game)
    score++;

    // Prepare text to be rendered.
    auto text = ("High Score: " + std::to_string(highScore) +
                 ",   Your Score: " + std::to_string(score)).c_str();

    // Render text to to texture and scale the destRect to fit the text.
    SDL_Surface *text_surface = TTF_RenderUTF8_Solid(font, text, COLOR);
    if (!text_surface) {
        cout << "Failed to render score text: " << TTF_GetError() << endl;
    } else {
        texture = SDL_CreateTextureFromSurface(renderer, text_surface);
        SDL_FreeSurface(text_surface);
        destRect.w = text_surface->w;
        destRect.h = text_surface->h;
    }
}

void Score::Render() {
    // Draw the score on the screen.
    SDL_RenderCopy(renderer, texture, nullptr, &destRect);
}
